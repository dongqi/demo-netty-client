﻿using System;

namespace demonettyclient
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Hello World!");
			//设定服务器IP地址  
			IPAddress ip = IPAddress.Parse("127.0.0.1");  
			Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

			clientSocket.Connect(new IPEndPoint(ip, 8090)); //配置服务器IP与端口  
			Console.WriteLine("连接服务器成功");  

			//通过 clientSocket 发送数据
			for (int index = 0; index < 100; index++) {
				SendReceiveTest(clientSocket);
			}

			Console.WriteLine("发送完毕，按回车键退出");  
			Console.ReadLine();
		}

		// Displays sending with a connected socket
		// using the overload that takes a buffer.
		public static int SendReceiveTest(Socket server)
		{
			String info = "Send 将数据同步发送到 Connect 或 Accept 方法中指定的远程主机，并返回成功发送的字节数。 Send 既可用于面向连接的协议，也可用于无连接协议。\n此重载要求一个缓冲区来包含要发送的数据。 SocketFlags 的默认值为 0，缓冲区偏移量的默认值为 0，发送字节数的默认值为缓冲区的大小。\n如果当前使用的是无连接协议，则必须先调用 Connect，然后才能调用此方法，否则 Send 将引发 SocketException。 如果您使用的面向连接的协议，则必须使用 Connect 建立远程主机连接，或者使用 Accept 接受传入的连接。\n如果您使用的是无连接协议，并且打算将数据发送到若干不同的主机，则应使用 SendTo 方法。 如果不使用 SendTo 方法，则每次调用 Send 之前必须调用 Connect。 即使已经用 Connect 建立了默认远程主机，也可以使用 SendTo。 通过另外调用 Connect，也可以在调用 Send 之前更改默认远程主机。\n如果您使用的是面向连接的协议，则除非使用 Socket.SendTimeout 设置了超时值，否则，Send 将一直处于阻止状态，直到发送完缓冲区中的所有字节。 如果超过超时值，Send 调用将引发 SocketException。 在非阻止模式下，Send 可能会成功完成，即使它发送的字节数小于缓冲区中的字节数。 应由您的应用程序负责跟踪已发送的字节数并重试操作，直到应用程序发送了缓冲区中的字节数为止。 不能保证发送的数据会立即出现在网络上。 为提高网络效率，基础系统可能会延迟传输，直到收集了足够多的传出数据后才开始发送。 Send 方法的成功完成意味着基础系统有空间来缓冲用于网络发送的数据。";
			byte[] msg = Encoding.UTF8.GetBytes(info);
			byte[] bytes = new byte[256];
			try 
			{
				// Blocks until send returns.
				int i = server.Send(msg);
				Console.WriteLine("Sent {0} bytes.", i);

				// Get reply from the server.
				i = server.Receive(bytes);
				Console.WriteLine(Encoding.UTF8.GetString(bytes));
			}
			catch (SocketException e)
			{
				Console.WriteLine("{0} Error code: {1}.", e.Message, e.ErrorCode);
				return (e.ErrorCode);
			}
			return 0;
		}
	}
}
